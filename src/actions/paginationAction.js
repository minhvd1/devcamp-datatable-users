import {
    CHANGE_PAGE_PAGINATION,
    FETCH_USERS_ERROR,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_PENDING
} from "../constants/paginationActionTypes";

export function changePagePagination(value) {
    return {
        type: CHANGE_PAGE_PAGINATION,
        payload: value
    };
}

export const fetchUsers = () => async dispatch => {

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }

    await dispatch({
        type: FETCH_USERS_PENDING
    });

    try {
        const response = await fetch(
            "https://jsonplaceholder.typicode.com/users", requestOptions
        );

        const data = await response.json();

        return dispatch({
            type: FETCH_USERS_SUCCESS,
            data: data
        });
    } catch (err) {
        return dispatch({
            type: FETCH_USERS_ERROR,
            error: err
        });
    }
};