import { Container, Grid, TableContainer, Paper, Table, TableHead, TableCell, TableRow, TableBody, Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changePagePagination, fetchUsers } from "../actions/paginationAction";

const Datatable = () => {
    const dispatch = useDispatch();

    const { currentPage, users, noPage } = useSelector((reduxData) => reduxData.paginationReducers);

    const changePageHandler = (event, value) => {
        dispatch(changePagePagination(value));
    }

    useEffect(() => {
        dispatch(fetchUsers());
    }, [currentPage]);

    return (
        <Container>
            <Grid container mt={5}>
                <Grid item style={{width: "100%"}} >
                    <TableContainer component={Paper} >
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell align="right">Name</TableCell>
                                <TableCell align="right">Username</TableCell>
                                <TableCell align="right">Email</TableCell>
                                <TableCell align="right">Phone</TableCell>
                                <TableCell align="right">Website</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                                {users.map((row) => (
                                    <TableRow
                                    key={row.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="right">{row.name}</TableCell>
                                    <TableCell align="right">{row.username}</TableCell>
                                    <TableCell align="right">{row.email}</TableCell>
                                    <TableCell align="right">{row.phone}</TableCell>
                                    <TableCell align="right">{row.website}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={1} onChange={changePageHandler}/>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Datatable;