import { combineReducers } from "redux";
import paginationReducers from "./paginationReducers";

const rootReducer = combineReducers({
    paginationReducers
});

export default rootReducer;