import { CHANGE_PAGE_PAGINATION, FETCH_USERS_ERROR, FETCH_USERS_PENDING, FETCH_USERS_SUCCESS } from "../constants/paginationActionTypes";

const limit = 3;

const initialState = {
    currentPage: 1,
    users: [],
    noPage: 0,
    pending: false,
    error: null
}

export default function paginationReducers(state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_PENDING:
            return {
              ...state,
              pending: true
            };
        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                users: action.data.slice((state.currentPage - 1) * limit, state.currentPage * limit),
                noPage: Math.ceil(action.data.length / limit),
                pending: false
            }
        case FETCH_USERS_ERROR:
            return {
                ...state,
                error: action.error,
                pending: false
            };
        case CHANGE_PAGE_PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
        default:
            return state;
    }
}